const { DynamoDBClient, QueryCommand } = require("@aws-sdk/client-dynamodb");


const codeVersion = '2020-10-27T145037|translucent-terrestrial-gerbil-of-culture';

queryDynamo = (options) => {
    const client = new DynamoDBClient();
    const query = new QueryCommand(options);

    return client.send(query).then(results => results.Items);
}

getAllFieldSets = () => {

    return queryDynamo({
        TableName: "fieldset_versions",
        IndexName :"code_version-index",
        KeyConditionExpression : "#id = :value",
        ExpressionAttributeNames:{
            "#id": "code_version"
        },
        ExpressionAttributeValues: {
            ":value": {S : codeVersion}
        }
        
    }).then(fieldSets => {
        console.debug(`got fieldsets count`,fieldSets.length);
        return fieldSets;
    });
}

getAllFields = () => {
    return queryDynamo({
        TableName: "field_versions",
        IndexName :"code_version-index",
        KeyConditionExpression : "#id = :value",
        ExpressionAttributeNames:{
            "#id": "code_version"
        },
        ExpressionAttributeValues: {
            ":value": {S : codeVersion}
        }
        
    }).then(fields => {
        console.debug(`got fields count`,fields.length);
        return fields;
    })
}

filterUniqueFieldSets = (value, index, self) => { 
    return self.findIndex(s => s.fieldset_id.S === value.fieldset_id.S) === index;
}

(async () => {

  try {
    const start = Date.now();
    console.debug(`querying fields`);

    const resolved = await Promise.all([getAllFields(),getAllFieldSets()]);
    const fields = resolved[0];
    const fieldSets = resolved[1].filter(filterUniqueFieldSets);

    console.debug(`got all fieldsets, going to filter and process`);
    for(const fieldSet of fieldSets) {
        const specifications = [];
        const tagIds = fieldSet.field_tag_ids.L;
        for(const fieldTag of tagIds) {
            const foundSpec = fields.find(fi => fi.field_id.S === fieldTag.S);
            if(foundSpec) {
                specifications.push(foundSpec.specification);
            }
        }
        fieldSet.specification = specifications;
    }

    console.debug(`completed processing ${fieldSets.length}`);

    const end = Date.now();
    const diff = end - start;
    console.debug(`total processing time is ${diff} ms`);
  } catch (err) {
    console.error(err);
  }
})();